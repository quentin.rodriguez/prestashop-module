<?php

declare(strict_types=1);

###################################################################
#                           Constants                             #
###################################################################

define("USERNAME", shell_exec("git config user.name 2> /dev/null"));
define("EMAIL", shell_exec("git config user.email 2> /dev/null"));

define("PROJECT_NAME", trim(basename(__DIR__)));
define("CLASS_NAME", ucwords(str_replace("-", "_", PROJECT_NAME), "_"));
define("FORMATTED_PROJECT_NAME", ucwords(str_replace(["_", "-"], " ", PROJECT_NAME)));
define("FORMATTED_NAMESPACE", ucwords(str_replace(["_", "-"], "\\", PROJECT_NAME), "\\"));
define("AUTOLOAD", ucwords(str_replace(["_", "-"], "\\\\", PROJECT_NAME), "\\\\"));

define("DIST_PATH", __DIR__."/dist");
define("SOURCE_PATH", __DIR__."/src");
define("INSTALL_PATH", __DIR__."/install.php");
define("GIT_PATH", __DIR__."/.git");


###################################################################
#                           Functions                             #
###################################################################

/**
 * @param string $template
 * @param array  $params
 */
function create_file(string $file, array $params = []): void 
{
    $template  = DIST_PATH."/$file.dist";
    $path      = $params["path"] ?? __DIR__;
    $filename  = $params["filename"] ?? $file;
    $variables = $params["variables"] ?? []; 

    $contents = str_replace(
        array_map(fn($key) => "{{ $key }}", array_keys($variables)),
        array_values($variables),
        file_get_contents($template)
    );
   
    file_put_contents("$path/$filename", $contents);
}

/**
 * @param string $dirname
 */
function rm_dir(string $dirname): void
{
    if (is_dir($dirname)) {
        $files = array_diff(scandir($dirname), [".", ".."]);
        
        foreach($files as $file) {
            is_dir("$dirname/$file") ? rm_dir("$dirname/$file") : rm_file("$dirname/$file");
        }

        @rmdir($dirname);
    }
}

/**
 * @param string $filename
 */
function rm_file(string $filename): void
{
    if (file_exists($filename)) {
        @unlink($filename);
    }
}


###################################################################
# Script:                                                         #
# Create file for operate module and remove file and directory    #
# of script with git folder                                       #
###################################################################

mkdir(SOURCE_PATH, 0755, true);

create_file("composer.json", [
    "variables" => [ 
        "NAME"       => PROJECT_NAME,
        "AUTOLOAD"   => AUTOLOAD,
        "USER_NAME"  => USERNAME,
        "USER_EMAIL" => EMAIL,
    ]
]);

create_file("AbstractModule.php", [
    "path"      => SOURCE_PATH,
    "variables" => [
        "NAMESPACE" => FORMATTED_NAMESPACE
    ]
]);

create_file("Module.php", [
    "filename"  => PROJECT_NAME.".php",
    "variables" => [
        "NAME"         => PROJECT_NAME,
        "CLASS_NAME"   => CLASS_NAME,
        "DISPLAY_NAME" => FORMATTED_PROJECT_NAME,
        "USER_NAME"    => USERNAME,
        "USER_EMAIL"   => EMAIL,
        "NAMESPACE"    => FORMATTED_NAMESPACE,
    ]
]);

rm_dir(GIT_PATH);
rm_dir(DIST_PATH);
rm_file(INSTALL_PATH);