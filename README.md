# Prestashop Module

Allows to facilitate all the steps of the installation of a module for Prestashop, while providing a panel of tools to manage the systems of hooks and also by providing a structure to arrange its code.

## Installation

Clone the project and give it a name, go to the folder and run the script for install the project
``` sh
git clone https://gitlab.com/quentin117/prestashop-module.git <MODULE NAME>
cd <MODULE_NAME>
php install.php
```

If you don't have a php version, you can use Docker
``` sh
docker run -it --rm -u $(id -u):$(id -g) -v $PWD:/home/$>(basename $PWD)  -w /home/$(basename $PWD) php:7.4 php install.php
```

## Features

- ### Hooks